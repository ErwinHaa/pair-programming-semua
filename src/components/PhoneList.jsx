import { Row, Spinner } from "reactstrap"
import { useSelector } from 'react-redux'

import { selectIsLoading, selectHasError } from '../store/reducers/phone'
import PhoneItem from "./PhoneItem"

const PhoneList = ({ phones }) => {
  const isLoading = useSelector(selectIsLoading)
  const hasError = useSelector(selectHasError);
  
  const renderedList = phones && phones.map(phone => <PhoneItem key={phone.phone_name} img={phone.phone_img_url} name={phone.phone_name} brand={phone.brand} slug={phone.phone_name_slug}/>)

  // const renderSpinner = isLoading ? <Spinner color="primary" /> : null;


  return (
    <div className="my-5 d-flex flex-column justify-content-center align-items-center">
      <Row xs="4" className="w-100 justify-content-center">
        {isLoading ? <div><Spinner color="primary"/></div> : renderedList && !hasError ? renderedList : <div className="w-50"><h2>No Phone Found</h2></div>}
      </Row>
    </div>
  )
}

export default PhoneList
