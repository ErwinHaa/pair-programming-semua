import { Route, Switch } from 'react-router-dom'

import HomePage from '../../pages/HomePage'
import BrandListing from '../../pages/BrandListing'
import PhoneDetail from '../../pages/PhoneDetail'

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route path="/brands">
        <BrandListing />
      </Route>
      <Route path="/phone-detail/:brandSlug/:nameSlug">
        <PhoneDetail />
      </Route>
    </Switch>
  )
}

export default AppRoutes
