import { useHistory } from 'react-router-dom'
import { Card, CardBody, CardText, CardTitle } from "reactstrap"

const PhoneItem = ({ img, name, brand, slug }) => {
  const history = useHistory();

  return (
    <Card className="col-3" onClick={() => history.push(`/phone-detail/${brand.toLowerCase()}/${slug}`)}>
    <CardBody>
      <CardTitle tag="h2">
        {name}
      </CardTitle>
    </CardBody>
      <img src={img} alt={name}/>
    <CardBody>
      <CardText>{brand}</CardText>
    </CardBody>
    </Card>
  )
}

export default PhoneItem
