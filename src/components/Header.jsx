import { Link } from 'react-router-dom'
import { Navbar, Nav, NavItem } from 'reactstrap'

import './Header.css'

const Header = () => {
  return (
    <Navbar expand="lg" color="dark" dark>
      <Nav>
        <NavItem>
          <div className="nav-link">
            <Link to="/">Beranda</Link>
          </div >
        </NavItem>
        <NavItem>
          <div className="nav-link">
            <Link to="/brands">Brand List</Link>
          </div>
        </NavItem>
      </Nav>
    </Navbar>
  )
}

export default Header

