import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Form, Input } from 'reactstrap'
import { fetchData } from '../store/thunk/index'

const SearchBar = () => {
  const dispatch = useDispatch();
  const [term, setTerm] = useState('');

  const onSearchSubmit = (e) => {
    e.preventDefault();
    dispatch(fetchData(term))
    setTerm('')
  }

  return (
    <div>
      Search for Phones
      <Form onSubmit={onSearchSubmit}>
        <Input type="text" value={term} onChange={(e) => setTerm(e.target.value)}/>
      </Form>
    </div>
  )
}

export default SearchBar
