import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useRouteMatch } from 'react-router-dom'
import { Table } from 'reactstrap'

import { fetchOne } from '../store/thunk'
import { selectPhone } from '../store/reducers/phone'

const PhoneDetail = () => {
  const match = useRouteMatch();

  const brand = match.params.brandSlug;
  const name = match.params.nameSlug;

  const dispatch = useDispatch();
  const phone = useSelector(selectPhone);

  useEffect(() => {
    dispatch(fetchOne(brand, name))
  }, [dispatch, brand, name])

  const renderedDetail = phone && <><h2>{phone.phone_name}</h2>
      <img src={phone.phone_img_url} alt={phone.phone_name} style={{ width:"100px" }} /> </>

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      {renderedDetail}
      <Table>
        <thead>
          <tr>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><b>{phone.specifications[0].title}</b></td>
          </tr>
          <tr>
            <td>{phone.specifications[0].specs[0].key}</td>
            <td>{phone.specifications[0].specs[0].val}</td>
          </tr>
          <tr>
            <td><b>{phone.specifications[1].title}</b></td>
          </tr>
          <tr>
            <td>{phone.specifications[1].specs[0].key}</td>
            <td>{phone.specifications[1].specs[0].val}</td>
          </tr>
          <tr>
            <td>{phone.specifications[1].specs[1].key}</td>
            <td>{phone.specifications[1].specs[1].val}</td>
          </tr>
        </tbody>
      </Table>
    </div>
  )
}

export default PhoneDetail
