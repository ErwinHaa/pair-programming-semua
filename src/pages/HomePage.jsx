import { useEffect } from 'react'
import { Container } from 'reactstrap'
import { useSelector, useDispatch } from 'react-redux'

import { selectPhones } from '../store/reducers/phone'
import { fetchData } from '../store/thunk'

import SearchBar from '../components/SearchBar'
import PhoneList from '../components/PhoneList'

const HomePage = () => {
  const phones = useSelector(selectPhones)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData('samsung'));
  }, [dispatch])


  return (
    <Container className="p-4">
      <SearchBar /> 
      <PhoneList phones={phones}/>
    </Container>
  )
}

export default HomePage
