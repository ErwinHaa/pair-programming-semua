import { useEffect, useState } from "react";
import { Route, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { FormGroup, Label, Input, Container } from 'reactstrap'
import PhoneList from "../components/PhoneList";

import { selectBrands, selectPhones } from '../store/reducers/phone'
import { fetchBrand, fetchData } from '../store/thunk'

const BrandListing = () => {
  const [selectedBrand, setSelectedBrand] = useState('');
  const dispatch = useDispatch();
  const brands = useSelector(selectBrands)
  const phones = useSelector(selectPhones)
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchBrand())
    if(selectedBrand) {
      dispatch(fetchData(selectedBrand))
      history.push(`/brands/${selectedBrand}`)
    }
  }, [dispatch, selectedBrand, history])
  
  const onSelectChange = e => {
    setSelectedBrand(e.target.value);
  }

  const renderOptions = brands.map(({ brand, brand_slug }) => <option key={brand}>{brand_slug}</option>)

  return (
    <Container>
    <FormGroup>
    <Label for="selectBrand">Select</Label>
      <Input type="select" id="selectBrand" value={selectedBrand} onChange={onSelectChange}>
        {renderOptions}
      </Input>

    </FormGroup>
      <Route path="/brands/:phoneBrand">
        <PhoneList phones={phones}/>
      </Route>
    </Container>
  )
}

export default BrandListing
