// Types constant

export const FETCH_DATA_START = 'FETCH_DATA_START';
export const FETCH_DATA_RESOLVED = 'FETCH_DATA_RESOLVED';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';
export const FETCH_BRAND = 'FETCH_BRAND';
export const FETCH_ONE = 'FETCH_ONE';

// Init state

const initialState = {
  phones: [],
  brands: [],
  selectedPhone: null,
  isLoading: false,
  hasError: false,
};

// Reducer

const phoneReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_DATA_START:
      return { ...state, isLoading: true, hasError: false };
    case FETCH_DATA_RESOLVED:
      return { ...state, isLoading: false, phones: payload };
    case FETCH_DATA_FAILURE:
      return { ...state, isLoading: false, phones: [], hasError: true };
    case FETCH_BRAND:
      return { ...state, brands: payload };
    case FETCH_ONE:
      return { ...state, selectedPhone: payload };
    default:
      return state;
  }
};

// Selectors

export const selectPhones = (state) => state.phones.phones;
export const selectIsLoading = (state) => state.phones.isLoading;
export const selectBrands = (state) => state.phones.brands;
export const selectPhone = (state) => state.phones.selectedPhone;
export const selectHasError = (state) => state.phones.hasError;

export default phoneReducer;
