import phone from '../../apis/phone';
import { fetchPhones } from '../actions';
import {
  FETCH_DATA_RESOLVED,
  FETCH_BRAND,
  FETCH_ONE,
  FETCH_DATA_FAILURE,
} from '../reducers/phone';

export const fetchData = (slug) => async (dispatch) => {
  dispatch(fetchPhones());
  try {
    const { data } = await phone.get(`/brands/${slug}`);

    return dispatch({ type: FETCH_DATA_RESOLVED, payload: data.data.phones });
  } catch (err) {
    dispatch({ type: FETCH_DATA_FAILURE });
  }
};

export const fetchBrand = () => async (dispatch) => {
  const { data } = await phone.get('/brands');

  return dispatch({ type: FETCH_BRAND, payload: data.data.brands });
};

export const fetchOne = (brand, name) => async (dispatch) => {
  const { data } = await phone.get(`/brands/${brand}/${name}`);

  return dispatch({ type: FETCH_ONE, payload: data.data });
};
