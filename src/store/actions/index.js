import { FETCH_DATA_START } from '../reducers/phone';

export const fetchPhones = () => {
  return { type: FETCH_DATA_START };
};
